# bash/ksh93 version

function arrayedit
{
    typeset opc s=p t v filter
    typeset -a dirs tmp
    typeset OPTIND
    while getopts 'A:F:S:ep' opc
    do
        case "$opc" in
        [AS])
            t=$opc
            v=$OPTARG
            ;;
        F)
            filter=$OPTARG
            ;;
        [ep])
            s=$opc
            ;;
        '?')
            echo >&2 "Usage: $0 [-ep] [-A array] [-S var] [editor…]"
            echo >&2 "  -e        - use Y7 escaped splitting for ‘-Svar’"
            echo >&2 "  -p        - use PATH-style splitting for ‘-Svar’"
            echo >&2 "  -A array  - display \${array[@]}"
            echo >&2 "  -S var    - display \${var}"
            return 1
            ;;
        esac
    done
    shift $((OPTIND - 1))
    if (($# == 0))
    then
        set -- ${VISUAL:-${EDITOR:-vi}}
    fi
    if [[ -z $v ]]
    then
        echo >&2 "$0: Use ‘-A array’ or ‘-S variable’"
        return 1
    fi

    typeset _split=y7${s}split _join=y7${s}join

    # Read variable.
    typeset -n V=$v
    if [[ $t == S ]]
    then
        $_split tmp "$V"
        dirs+=("${tmp[@]}")
    else
        dirs+=("${V[@]}")
    fi

    # Write to file.
    typeset d tmp=${TMPDIR:-/tmp}/dirsedit:$$
    for d in "${dirs[@]}"
    do
        printf '%s\n' "$d"
    done >$tmp

    if ! eval "$@" "$tmp"
    then
        typeset r=$?
        echo >&2 "$0: No change"
        return $r
    fi

    # Read from file.
    dirs=()
    if [[ $v == Y7DIRS ]]
    then
        dirs+=("$PWD")
    fi
    while read -r d
    do
        if [[ -n $d ]]
        then
            dirs+=("$d")
        fi
    done <$tmp

    if [[ -n $filter ]]
    then
        $filter dirs
    fi

    # Write variable.
    if [[ $t == S ]]
    then
        V=$($_join "${dirs[@]}")
    else
        V=("${dirs[@]}")
    fi
}
